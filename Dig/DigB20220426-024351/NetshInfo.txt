
Idx     Met         MTU          State                Name
---  ----------  ----------  ------------  ---------------------------
  1          75  4294967295  connected     Loopback Pseudo-Interface 1
  6           5        1500  connected     Ethernet


Interface Loopback Pseudo-Interface 1 Parameters
----------------------------------------------
IfLuid                             : loopback_0
IfIndex                            : 1
State                              : connected
Metric                             : 75
Link MTU                           : 4294967295 bytes
Reachable Time                     : 28000 ms
Base Reachable Time                : 30000 ms
Retransmission Interval            : 1000 ms
DAD Transmits                      : 0
Site Prefix Length                 : 64
Site Id                            : 1
Forwarding                         : disabled
Advertising                        : disabled
Neighbor Discovery                 : disabled
Neighbor Unreachability Detection  : disabled
Router Discovery                   : dhcp
Managed Address Configuration      : enabled
Other Stateful Configuration       : enabled
Weak Host Sends                    : disabled
Weak Host Receives                 : disabled
Use Automatic Metric               : enabled
Ignore Default Routes              : disabled
Advertised Router Lifetime         : 1800 seconds
Advertise Default Route            : disabled
Current Hop Limit                  : 0
Force ARPND Wake up patterns       : disabled
Directed MAC Wake up patterns      : disabled
ECN capability                     : application
RA Based DNS Config (RFC 6106)     : disabled
DHCP/Static IP coexistence         : disabled


Interface Ethernet Parameters
----------------------------------------------
IfLuid                             : ethernet_32769
IfIndex                            : 6
State                              : connected
Metric                             : 5
Link MTU                           : 1500 bytes
Reachable Time                     : 39500 ms
Base Reachable Time                : 30000 ms
Retransmission Interval            : 1000 ms
DAD Transmits                      : 3
Site Prefix Length                 : 64
Site Id                            : 1
Forwarding                         : disabled
Advertising                        : disabled
Neighbor Discovery                 : enabled
Neighbor Unreachability Detection  : enabled
Router Discovery                   : dhcp
Managed Address Configuration      : enabled
Other Stateful Configuration       : enabled
Weak Host Sends                    : disabled
Weak Host Receives                 : disabled
Use Automatic Metric               : enabled
Ignore Default Routes              : disabled
Advertised Router Lifetime         : 1800 seconds
Advertise Default Route            : disabled
Current Hop Limit                  : 0
Force ARPND Wake up patterns       : disabled
Directed MAC Wake up patterns      : disabled
ECN capability                     : application
RA Based DNS Config (RFC 6106)     : disabled
DHCP/Static IP coexistence         : disabled

@{MacAddress=00-22-48-9C-BD-68; Status=Up; LinkSpeed=100 Gbps; MediaType=802.3; PhysicalMediaType=Unspecified; AdminStatus=Up; MediaConnectionState=Connected; DriverInformation=Driver Date 2006-06-21 Version 10.0.17763.2300 NDIS 6.70; DriverFileName=netvsc.sys; NdisVersion=6.70; ifOperStatus=Up; ifAlias=Ethernet; InterfaceAlias=Ethernet; ifIndex=6; ifDesc=Microsoft Hyper-V Network Adapter; ifName=ethernet_32769; DriverVersion=10.0.17763.2300; LinkLayerAddress=00-22-48-9C-BD-68; Caption=; Description=; ElementName=; InstanceID={EEFBBD26-7815-4DD2-9FAC-0BF77CCDCDF9}; CommunicationStatus=; DetailedStatus=; HealthState=; InstallDate=; Name=Ethernet; OperatingStatus=; OperationalStatus=; PrimaryStatus=; StatusDescriptions=; AvailableRequestedStates=; EnabledDefault=2; EnabledState=5; OtherEnabledState=; RequestedState=12; TimeOfLastStateChange=; TransitioningToState=12; AdditionalAvailability=; Availability=; CreationClassName=MSFT_NetAdapter; DeviceID={EEFBBD26-7815-4DD2-9FAC-0BF77CCDCDF9}; ErrorCleared=; ErrorDescription=; IdentifyingDescriptions=; LastErrorCode=; MaxQuiesceTime=; OtherIdentifyingInfo=; PowerManagementCapabilities=; PowerManagementSupported=; PowerOnHours=; StatusInfo=; SystemCreationClassName=CIM_NetworkPort; SystemName=APPAD1.adaudit77.lt; TotalPowerOnHours=; MaxSpeed=; OtherPortType=; PortType=; RequestedSpeed=; Speed=100000000000; UsageRestriction=; ActiveMaximumTransmissionUnit=1500; AutoSense=; FullDuplex=; LinkTechnology=; NetworkAddresses=System.String[]; OtherLinkTechnology=; OtherNetworkPortType=; PermanentAddress=0022489CBD68; PortNumber=0; SupportedMaximumTransmissionUnit=; AdminLocked=False; ComponentID=VMBUS\{f8615163-df3e-46c5-913f-f2d2f965ed0e}; ConnectorPresent=True; DeviceName=\Device\{EEFBBD26-7815-4DD2-9FAC-0BF77CCDCDF9}; DeviceWakeUpEnable=False; DriverDate=2006-06-21; DriverDateData=127953216000000000; DriverDescription=Microsoft Hyper-V Network Adapter; DriverMajorNdisVersion=6; DriverMinorNdisVersion=70; DriverName=\SystemRoot\System32\drivers\netvsc.sys; DriverProvider=Microsoft; DriverVersionString=10.0.17763.2300; EndPointInterface=False; HardwareInterface=True; Hidden=False; HigherLayerInterfaceIndices=System.UInt32[]; IMFilter=False; InterfaceAdminStatus=1; InterfaceDescription=Microsoft Hyper-V Network Adapter; InterfaceGuid={EEFBBD26-7815-4DD2-9FAC-0BF77CCDCDF9}; InterfaceIndex=6; InterfaceName=ethernet_32769; InterfaceOperationalStatus=1; InterfaceType=6; iSCSIInterface=False; LowerLayerInterfaceIndices=; MajorDriverVersion=10; MediaConnectState=1; MediaDuplexState=0; MinorDriverVersion=0; MtuSize=1500; NdisMedium=0; NdisPhysicalMedium=0; NetLuid=1689399632855040; NetLuidIndex=32769; NotUserRemovable=False; OperationalStatusDownDefaultPortNotAuthenticated=False; OperationalStatusDownInterfacePaused=False; OperationalStatusDownLowPowerState=False; OperationalStatusDownMediaDisconnected=False; PnPDeviceID=VMBUS\{f8615163-df3e-46c5-913f-f2d2f965ed0e}\{0022489c-bd68-0022-489c-bd680022489c}; PromiscuousMode=False; ReceiveLinkSpeed=100000000000; State=2; TransmitLinkSpeed=100000000000; Virtual=False; VlanID=0; WdmInterface=False; PSComputerName=; CimClass=ROOT/StandardCimv2:MSFT_NetAdapter; CimInstanceProperties=Microsoft.Management.Infrastructure.Internal.Data.CimPropertiesCollection; CimSystemProperties=Microsoft.Management.Infrastructure.CimSystemProperties}
@{Profile=NUMAStatic; ifAlias=Ethernet; InterfaceAlias=Ethernet; ifDesc=Microsoft Hyper-V Network Adapter; Caption=MSFT_NetAdapterRssSettingData 'Microsoft Hyper-V Network Adapter'; Description=Microsoft Hyper-V Network Adapter; ElementName=Microsoft Hyper-V Network Adapter; InstanceID={EEFBBD26-7815-4DD2-9FAC-0BF77CCDCDF9}; InterfaceDescription=Microsoft Hyper-V Network Adapter; Name=Ethernet; Source=2; SystemName=APPAD1.adaudit77.lt; BaseProcessorGroup=0; BaseProcessorNumber=0; ClassificationAtDpcSupported=False; ClassificationAtIsrSupported=False; Enabled=True; HashKeySize=40; IndirectionTable=Microsoft.Management.Infrastructure.CimInstance[]; IndirectionTableEntryCount=128; IPv4HashEnabled=True; IPv6ExtensionHashEnabled=False; IPv6HashEnabled=True; MaxProcessorGroup=0; MaxProcessorNumber=3; MaxProcessors=4; MsiSupported=False; MsiXEnabled=False; MsiXSupported=False; NumaNode=0; NumberOfInterruptMessages=1; NumberOfReceiveQueues=4; RssOnPortsSupported=True; RssProcessorArray=Microsoft.Management.Infrastructure.CimInstance[]; RssProcessorArraySize=4; TcpIPv4HashEnabled=True; TcpIPv4HashSupported=True; TcpIPv6ExtensionHashEnabled=False; TcpIPv6ExtensionHashSupported=False; TcpIPv6HashEnabled=True; TcpIPv6HashSupported=True; ToeplitzHashFunctionEnabled=True; ToeplitzHashFunctionSupported=True; UdpIPv4HashEnabled=False; UdpIPv4HashSupported=False; UdpIPv6ExtensionHashEnabled=False; UdpIPv6ExtensionHashSupported=False; UdpIPv6HashEnabled=False; UdpIPv6HashSupported=False; PSComputerName=; CimClass=ROOT/StandardCimv2:MSFT_NetAdapterRssSettingData; CimInstanceProperties=Microsoft.Management.Infrastructure.Internal.Data.CimPropertiesCollection; CimSystemProperties=Microsoft.Management.Infrastructure.CimSystemProperties}
@{ifAlias=Ethernet; InterfaceAlias=Ethernet; ifDesc=Microsoft Hyper-V Network Adapter; PFC=NA; ETS=NA; Caption=MSFT_NetAdapterRdmaSettingData 'Microsoft Hyper-V Network Adapter'; Description=Microsoft Hyper-V Network Adapter; ElementName=Microsoft Hyper-V Network Adapter; InstanceID={EEFBBD26-7815-4DD2-9FAC-0BF77CCDCDF9}; InterfaceDescription=Microsoft Hyper-V Network Adapter; Name=Ethernet; Source=3; SystemName=APPAD1.adaudit77.lt; Enabled=False; MaxCompletionQueueCount=; MaxInboundReadLimit=; MaxMemoryRegionCount=; MaxMemoryWindowCount=; MaxOutboundReadLimit=; MaxProtectionDomainCount=; MaxQueuePairCount=; MaxSharedReceiveQueueCount=; RdmaAdapterInfo=; RdmaMissingCounterInfo=; PSComputerName=; CimClass=ROOT/StandardCimv2:MSFT_NetAdapterRdmaSettingData; CimInstanceProperties=Microsoft.Management.Infrastructure.Internal.Data.CimPropertiesCollection; CimSystemProperties=Microsoft.Management.Infrastructure.CimSystemProperties}


# ----------------------------------
# IPv4 Configuration
# ----------------------------------
pushd interface ipv4

reset
set global
set interface interface="Ethernet (Kernel Debugger)" forwarding=enabled advertise=enabled nud=enabled ignoredefaultroutes=disabled
set interface interface="Ethernet" forwarding=enabled advertise=enabled nud=enabled ignoredefaultroutes=disabled


popd
# End of IPv4 configuration


